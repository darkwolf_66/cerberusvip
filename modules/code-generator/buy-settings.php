<?php

if(!isset($_GET['id']) && is_int($_GET['id'])){
    header('location:'.$config->url->home);
}
$id = $_GET['id'];

// Create connection
$conn = new mysqli($config->mysql->host, $config->mysql->user, $config->mysql->password, $config->mysql->database);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$stmt = $conn->prepare("SELECT
cb_products.`server` as pr_server,
cb_products.price as pr_price,
cb_products.`name` as pr_name,
cb_products.image_url as pr_image_url,
cb_products.id_product as pr_id,
cb_products.description as pr_description,
cb_products.currency as pr_currency,
cb_products.expire as pr_expire,
cb_products.visibility as pr_visibility,
cb_currencies.prefix as cur_prefix,
cb_currencies.`name` as cur_name,
cb_currencies.id_currency as cur_id,
cb_servers.`name` as cb_server_name,
cb_servers.is_global as cb_server_is_global,
cb_servers.image_url as cb_server_image_url,
cb_servers.visibility as cb_server_visibility
FROM cb_products
INNER JOIN cb_currencies ON cb_products.currency = cb_currencies.id_currency
INNER JOIN cb_servers ON cb_products.`server` = cb_servers.server_id
WHERE cb_products.id_product = ?");

$stmt->bind_param('s',$id);
$stmt->execute();
$result = $stmt->get_result();
if ($result->num_rows <= 0) {
    header('location:'.$config->url->home);
}
$row = $result->fetch_array();
if($row['pr_expire'] == 1){
    $stmt_time = $conn->prepare("SELECT * FROM cb_products_time WHERE product = ?");
    $stmt_time->bind_param('s',$row['pr_id']);
    $stmt_time->execute();
    $result_time = $stmt_time->get_result();
}
$stmt_pay = $conn->prepare("SELECT * FROM cb_product_gateway
INNER JOIN cb_gateway ON cb_product_gateway.gateway = cb_gateway.id_gateway
WHERE product = ?");
$stmt_pay->bind_param('s',$row['pr_id']);
$stmt_pay->execute();
$result_pay = $stmt_pay->get_result();
?>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                <?php

                if($row['pr_expire'] == 1){
                    echo '<div class="box"><h3 class="box-title">'.$string->select_duration_time.'</h3>';
                    $checked = ' checked="checked"';
                    while ($row_time = $result_time->fetch_array()) {
                        echo '<div class="plan-selection">
                            <div class="plan-data">
                                <input type="radio" name="time" value="'.$row_time['id_product_time'].'" '.$checked.'>
                                <label for="question4">'.$row_time['name'].'</label>
                                <span class="plan-price term-price">'.$row['cur_prefix'].$row_time['price'].'</span>
                            </div>
                        </div>';
                        $checked = '';
                    }
                     echo '</div>';
                }

                ?>
                <div class="box">
                    <h3 class="box-title"><?php echo $string->payment_gateway; ?></h3>
                    <div class="row justify-content-center">
                        <div class="col-10">
                            <?php
                            $checked = ' checked="checked"';

                            while ($row_pay = $result_pay->fetch_array()) {
                                if($row_pay['status'] == 1){
                                    echo '<div class="plan-selection">
                                    <div class="plan-data">
                                        <label class="buy-page-payment-gateway">
                                          <input type="radio" name="payment-gateway" value="'.$row_pay['id_product_gateway'].'"'.$checked.'>
                                          <img class="img-fluid" src="'.$row_pay['image_description'].'">
                                        </label>
                                    </div>
                                </div>';
                                    $checked = "";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary btn-lg mb30 checkout"><?php echo $string->checkout; ?></button>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                <div class="widget">
                    <h4 class="widget-title"><?php echo $string->order_details; ?></h4>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><img class="img-fluid" src="<?php echo $row['cb_server_image_url']?>"></span>
                        </div>
                        <input type="text" class="form-control text-center" value="<?php echo $string->server.' '.$row['cb_server_name']?>" readonly>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><img class="img-fluid buy-page-product-image" src="<?php echo $row['pr_image_url']?>"></span>
                            </div>
                            <input type="text" class="form-control text-center" value="<?php echo $row['pr_name']; ?>" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$stmt->close();
$conn->close();
?>