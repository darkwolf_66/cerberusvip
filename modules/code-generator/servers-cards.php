<div class="container">
    <div class="row servers">

<?php

// Create connection
$conn = new mysqli($config->mysql->host, $config->mysql->user, $config->mysql->password, $config->mysql->database);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
    header('location:'.$config->url->home);
    return 0;
}

$sql = "SELECT * FROM cb_servers";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo ' <div class="col-md-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="'.$row["image_url"].'">
                <div class="card-body">
                    <h5 class="card-title col-md-12 text-center">'.$row["name"].'</h5>
                    <p class="card-text">'.$row["description"].'</p>
                    <div class="col-md-12 text-center"><a href="server.php?id='.$row["server_id"].'" class="btn btn-primary">'.$string->see_server.'</a></div>
                </div>
            </div>
        </div>';
    }
} else {
    echo "0 results";
}
$conn->close();
?>
    </div>
</div>
