
<div class="container">
    <div class="row products">
<?php

if(!isset($_GET['id']) && is_int($_GET['id'])){
    header('location:'.$config->url->home);
}
$id = $_GET['id'];

// Create connection
$conn = new mysqli($config->mysql->host, $config->mysql->user, $config->mysql->password, $config->mysql->database);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
    header('location:'.$config->url->home);
    return 0;
}
$stmt = $conn->prepare("SELECT * FROM cb_products WHERE server = ?");
$stmt->bind_param('s',$id);
$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo ' <div class="col-md-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="'.$row["image_url"].'">
                <div class="card-body">
                    <h5 class="card-title col-md-12 text-center">'.$row["name"].'</h5>
                    <p class="card-text">'.$row["description"].'</p>
                    <div class="col-md-12 text-center"><a href="buy.php?id='.$row["id_product"].'" class="btn btn-primary">'.$string->buy.'</a></div>
                </div>
            </div>
        </div>';
    }
} else {
    echo "0 results";
}
$stmt->close();
$conn->close();
?>
    </div>
</div>
