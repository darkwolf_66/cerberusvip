<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <title><?php echo $config->details->name; ?></title>
    <!-- Bootstrap core CSS -->
    <link href="css/tether.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/fontawesome.min.css" rel="stylesheet">
    <link href="modules/code-generator/styles.css.php" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
