<?php
$config = (object)[
    'details' => (object)[
        'name' => 'CerberusStore',
        'description' => 'An simple demonstration store.',
        'language' => 'pt-br',
        'database' => 'cerberusvip',
        'iframe_mode' => true
    ],
    'url' => (object)[
        'cerberus'=>'cerberus.ml',
        'home' => 'http://cerberusvip.io'
    ],
    'mysql'=> (object)[
        'host' => '127.0.0.1',
        'user' => 'root',
        'password' => '123',
        'database' => 'cerberusvip'
    ]
];
