<?php
require_once 'config.php';
require_once 'modules/language/'.$config->details->language.'.php';
?>
<?php
//Generate MainHead
require_once 'modules/code-resources/main-head.php';
?>
<body>
<?php
//Generate Menu
require_once 'modules/code-resources/main-navbar.php';
//
//  < Page Content
//
//Generate Server Cards
require_once 'modules/code-generator/buy-settings.php';
//
//  Page Content End >
//
//Generate Main Footer
require_once 'modules/code-resources/main-footer.php';
?>
</body>
<?php
//Generate Main Js Includes
require_once 'modules/code-resources/main-js-includes.php';
?>
<script><?php require_once 'modules/code-generator/buy-settings.form.js.php'; ?></script>
</html>
